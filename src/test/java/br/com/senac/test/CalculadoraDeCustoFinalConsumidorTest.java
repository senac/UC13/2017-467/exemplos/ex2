/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.ex2.CalculadoraDeCustoFinalConsumidor;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class CalculadoraDeCustoFinalConsumidorTest {
    
    public CalculadoraDeCustoFinalConsumidorTest() {
    }
    
    @Test
    public void deveCalcularCustoFinalConsumidor(){
        
        CalculadoraDeCustoFinalConsumidor calculadora = new CalculadoraDeCustoFinalConsumidor();
        double valorCusto = 10000 ; 
        double resultado = calculadora.calcular(valorCusto) ; 
        
        assertEquals(17300, resultado , 0.01);
        
    }
    
}
